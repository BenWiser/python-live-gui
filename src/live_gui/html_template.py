import pathlib
import json

from string import Template

class HtmlTemplate:
    """ This class is just responsible for
    rendering out the page when you first load

    It will embed the javascript and styles and set a few
    settings that will be used for syncing
    """

    __slots__ = ("template", "javascript", "style")

    def __init__(self):
        template_path = pathlib.Path(__file__).parent.parent / 'template'

        def open_template(template):
            return open((template_path / template).as_posix())

        with open_template('page.html') as html_raw, open_template('javascript.js') as javascript, open_template('style.css') as style:
            self.template = Template(html_raw.read())
            self.javascript = javascript.read()
            self.style = style.read()

    def render(self, app_name: str, content: str, settings: dict) -> str:
        return self.template.substitute({
            'app_name': app_name,
            'content': content,
            'settings': json.dumps(settings),
            'javascript': self.javascript,
            'style': self.style,
        })