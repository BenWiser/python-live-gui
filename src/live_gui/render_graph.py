from dataclasses import dataclass
from collections import namedtuple
from typing import Any

from components import ComponentManager

@dataclass
class GraphNode:
    node_id: int
    render_id: int
    parent: Any
    component: Any
    children: Any
    render: str
    render_closing: str

class RenderGraph:
    """ RenderGraph is the data structure for the render
    calls made by client apps

    It also tracks if any rerenders need to occur and returns
    the html that will be rendered
    """
    def __init__(self):
        self.init()
        self._previous_renders = {}
        self._next_renders = {}

        self.component_manager = ComponentManager()

        # Auto create APIs for each component
        for component in self.component_manager.iter():
            setattr(self, 'add_%s' % component.get_name(), self._add_component(component))
            setattr(self, 'open_%s_children' % component.get_name(), self._open_children(component))
            setattr(self, 'close_%s_children' % component.get_name(), self._close_children(component))
    
    def _get_id(self, child_id, component_name: str) -> str:
        # We know that the component has not changed if it is rendered in the
        # same order and if it has the same id
        id = component_name + "-" + str(child_id)
        parent = self._current_node
        while True:
            id += "-" + str(parent.node_id)
            parent = parent.parent
            if parent is None:
                break
        return id

    def init(self):
        self._current_node = GraphNode(
            node_id = 0,
            render_id = 'root',
            parent = None,
            component = None,
            children = [],
            render = '<div id="root">',
            render_closing = '</div>',
        )
    
    def reset(self):
        self.component_manager.reset()

    def perform_render(self):
        root_node = self._current_node
        while root_node.parent is not None:
            root_node = root_node.parent

        renders = self._perform_child_renders(root_node)
        self._previous_renders = self._next_renders
        self._previous_renders['root'] = root_node
        self._next_renders = {}

        return renders

    def _perform_child_renders(self, node):
        def getRenderIdsOfChildren(node):
            return [child.render_id for child in node.children]

        renders = []

        nodes_to_render = [node]

        for node_to_render in nodes_to_render:
            render_self = False

            if not self._previous_renders.get(node_to_render.render_id):
                render_self = True
            elif node_to_render.component != self._previous_renders[node_to_render.render_id].component:
                render_self = True
            elif getRenderIdsOfChildren(node_to_render) != getRenderIdsOfChildren(
                self._previous_renders[node_to_render.render_id]):
                render_self = True

            if render_self:
                renders += [{
                    'id': node_to_render.render_id,
                    'value': node_to_render.render + node_to_render.render_closing
                }]
            else:
                nodes_to_render += node_to_render.children

        return renders

    def _add_component(self, component):
        def inner_add_component(*argv):
            node_id = self._current_node.children[-1].node_id + 1 if self._current_node.children else 0
            render_id = self._get_id(node_id, component.get_name())

            action, graph_component = component.add_to_render_graph(render_id, *argv)
            rendered = component.render(render_id, graph_component)

            graph_node = GraphNode(
                node_id = node_id,
                render_id = render_id,
                parent = self._current_node,
                component = graph_component,
                children = [],
                render = rendered,
                render_closing = '',
            )

            self._current_node.children += [graph_node]
            self._current_node.render += rendered

            self._next_renders[render_id] = graph_node

            return action

        return inner_add_component
    
    def _open_children(self, component):
        def inner_open_children(*argv):
            node_id = self._current_node.children[-1].node_id + 1 if self._current_node.children else 0
            render_id = self._get_id(node_id, component.get_name())

            parent = self._current_node

            action, graph_component = component.add_to_render_graph(render_id, *argv)
            render_generator = component.render(render_id, graph_component)

            self._current_node = GraphNode(
                node_id = node_id,
                render_id = render_id,
                parent = parent,
                component = graph_component,
                children = [],
                render = render_generator.__next__(),
                render_closing = render_generator.__next__(),
            )

            parent.children.append(self._current_node)

            self._next_renders[render_id] = self._current_node

            return action

        return inner_open_children

    def _close_children(self, component):
        def inner_close_children():
            if self._current_node.component.name != component.get_name():
                raise Exception('Expected to close with %s but got %s' % (
                    self._current_node.component.name, component.get_name()))

            self._current_node.parent.render += (
                self._current_node.render +
                self._current_node.render_closing)

            self._current_node = self._current_node.parent

        return inner_close_children