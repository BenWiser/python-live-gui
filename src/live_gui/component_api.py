from contextlib import contextmanager

from app_state import APP_MANAGER

def button(text: str) -> bool:
    """ Call this method to render a button

    It will return True when the button is pressed
    """
    return APP_MANAGER.get_current_app()._button(text)

def label_input(label: str, default: str) -> str:
    """ Call this method to render a label input

    It will show a label before the input and return any updates
    """
    return APP_MANAGER.get_current_app()._label_input(label, default)

def text(text: str) -> None:
    """ Call this method to render a paragraph
    """
    return APP_MANAGER.get_current_app()._text(text)

@contextmanager
def column():
    """ Used in a with statement to
    put children elements in a column
    """
    app = APP_MANAGER.get_current_app()
    app._start_column()
    yield
    app._end_column()

@contextmanager
def row():
    """ Used in a with statement to
    put children elements in a row
    """
    app = APP_MANAGER.get_current_app()
    app._start_row()
    yield
    app._end_row()

@contextmanager
def a_list():
    """ Used in a with statement to
    put children elements in a list
    """
    app = APP_MANAGER.get_current_app()
    app._start_list()
    yield
    app._end_list()

@contextmanager
def list_item():
    """ Used in a with statement to
    put children elements in a list item
    """
    app = APP_MANAGER.get_current_app()
    app._start_list_item()
    yield
    app._end_list_item()

@contextmanager
def menu_bar():
    """ Used in a with statement to
    put children elements in a menu bar
    Used with buttons
    """
    app = APP_MANAGER.get_current_app()
    app._start_menu_bar()
    yield
    app._end_menu_bar()
