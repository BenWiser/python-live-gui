from app_state import APP_MANAGER
from render_graph import RenderGraph

class ServerState:
    """ This small class is used to manage
    the different apps per connection
    """

    __slots__ = ("_apps", "_app_class", "_current_max_response_id")

    def __init__(self, app_class):
        self._apps = {}
        self._app_class = app_class
        self._current_max_response_id = 0
    
    def add_response(self) -> int:
        response_id = self._current_max_response_id
        render_graph = RenderGraph()
        self._apps[response_id] = self._app_class(render_graph)
        self._current_max_response_id += 1
        APP_MANAGER.add_app(response_id, self._apps[response_id])
        return response_id

    def get_app(self, id: int):
        return self._apps[id]

    def delete_id(self, id: int):
        del self._apps[id]