from components.component import Component, GraphComponent

class Row(Component):
    def get_name(self) -> str:
        return 'row'

    def render(self, id: int, button) -> str:
        yield '<div id="{0}" class="row">'.format(id)
        yield '</div>'
