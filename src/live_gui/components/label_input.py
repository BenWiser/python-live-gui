from components.component import Component, GraphComponent

class LabelInput(Component):

    __slots__ = ("label_inputs")

    def __init__(self):
        self.label_inputs = {}

    def get_name(self) -> str:
        return 'label_input'

    def add_to_render_graph(self, id: int, label: str, default: str):
        action = self.get_action(id, default)

        return action, GraphComponent(
            render_id = id,
            name = self.get_name(),
            args = (label, default))

    def reset(self) -> None:
        self.label_inputs = {}

    def do_action(self, id: int, value: str) -> None:
        self.label_inputs[id] = value
    
    def get_action(self, id: int, default: str):
        return self.label_inputs.get(id, default)

    def render(self, id: int, label_input) -> str:
        return (
            '<div id="{0}" class="label">'
            '<small>{3}</small>'
            '<input onChange="doAction(\'{1}\',\'{2}\', event.target.value)" value="{4}" autocomplete="off" />'
            '</div>'
        ).format(id, self.get_name(), label_input.render_id, label_input.args[0], label_input.args[1])
