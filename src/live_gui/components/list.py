from components.component import Component, GraphComponent

class List(Component):
    def get_name(self) -> str:
        return 'list'

    def render(self, id: int, button) -> str:
        yield '<div class="list" id="{0}"><ul>'.format(id)
        yield '</ul></div>'
