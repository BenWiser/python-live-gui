from components.component import Component, GraphComponent

class ListItem(Component):
    def get_name(self) -> str:
        return 'list_item'

    def render(self, id: int, button) -> str:
        yield '<li class="list_item" id="{0}">'.format(id)
        yield '</li>'
