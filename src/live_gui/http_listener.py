
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from time import sleep
from threading import Lock

import socket
import json
import signal

from base_app import App
from server_state import ServerState
from html_template import HtmlTemplate
from app_state import APP_MANAGER

# This is how often the render method is called
# to perform updates in seconds
# Anything around 100ms should be fine to feel responsive
# Went for 80ms to give programs a little wiggle room to render
PERFORM_UPDATES_S = 0.08


# We can only know that the connection is dead if we send some data
# and it failed so we occasionally need to do a "pulse"
# By default, render response won't send anything if it has
# nothing to render for efficiency reasons
# send an empty body if this needs a health check
FORCE_HEALTH_TESTS=50

class _HttpListener(BaseHTTPRequestHandler):
    """ HttpListener is responsible for maintaining
    connections to clients and syncing updates

    The actual state should not be managed in this class
    """

    def _render_response(self, app, skip_render=True):
        rendered_content = app._private_render()

        if (len(rendered_content) > 0):
            self.wfile.write(json.dumps({
                'body': rendered_content
            }).encode())
        elif not skip_render:
            self.wfile.write(b'{}')

    def do_POST(self):
        self.send_response(200)
        self.end_headers()
        
        content_len = int(self.headers.get('Content-Length'))
        action = json.loads(self.rfile.read(content_len).decode("utf-8"))
        response_id = action['response_id']
        APP_MANAGER.set_response_id_for_current_thread(response_id)

        with self.server.render_lock:
            app = self.server.server_state.get_app(response_id)
            app.render_graph.reset()

            component = getattr(app.render_graph.component_manager, action['action'])
            component.do_action(*action.get('args'))

            self._render_response(app, False)

            APP_MANAGER.unset_response_id_for_current_thread()

    def _handle_initial_load(self):
        self.end_headers()

        response_id = self.server.server_state.add_response()
        app = self.server.server_state.get_app(response_id)
        APP_MANAGER.set_response_id_for_current_thread(response_id)

        app.render_graph.reset()
        # On first load, we should just the parent back
        body = app._private_render()[0]['value']

        template = HtmlTemplate()
        html_response = template.render(
            app_name = self.server.app_name,
            content = body,
            settings = {
                'port': self.server.server_port,
                'app_name': self.server.app_name,
                'response_id': response_id,
            }
        )
        self.wfile.write(html_response.encode())
        APP_MANAGER.unset_response_id_for_current_thread()
            

    def _handle_updates(self, parameters):
        # This is to fix a streaming response bug in firefox
        # that makes the first response take ages for the updates
        self.send_header("Content-Type", "text/html")
        self.end_headers()

        params = parameters.split('&', 10)
        response_id = -1
        for param in params:
            name, value = param.split('=')
            if name == 'response_id':
                response_id = int(value)

        app = self.server.server_state.get_app(response_id)
        APP_MANAGER.set_response_id_for_current_thread(response_id)

        health_test = 0

        try:
            while True:
                with self.server.render_lock:
                    if not self.server.is_alive:
                        self.wfile.write(b'{}')
                        break

                    app.render_graph.reset()

                    health_test += 1
                    should_do_health_check = health_test >= FORCE_HEALTH_TESTS

                    self._render_response(app, not should_do_health_check)

                    if should_do_health_check:
                        health_test = 0

                sleep(PERFORM_UPDATES_S)
        except socket.error:
            self.server.server_state.delete_id(response_id)
            print('Client disconnected')
            APP_MANAGER.unset_response_id_for_current_thread()

    def do_GET(self):
        self.send_response(200)

        path = self.path.split("?")

        if path[0] == "/":
            self._handle_initial_load()
        elif path[0] == "/updates":
            self._handle_updates(path[1])


    def log_message(self, format, *args):
        # Quiets the logs
        return

class _ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def startApp(app_name: str, app_class, port: int) -> None:
    """ Call this method to start rendering
    """

    if not issubclass(app_class, App):
        raise Exception("%s does not inherit from PythonLiveApp" % app_class.__name__)

    httpd = _ThreadedHTTPServer(('localhost', port), _HttpListener)
    httpd.server_state = ServerState(app_class)
    httpd.render_lock = Lock()
    httpd.app_name = app_name
    httpd.is_alive = True

    def shutdown_gracefull(*args):
        httpd.is_alive = False
        httpd.server_close()
        print("Shutting down")

    signal.signal(signal.SIGINT, shutdown_gracefull)
    signal.signal(signal.SIGTERM, shutdown_gracefull)

    print('Starting on http://127.0.0.1:{0}'.format(port))

    while httpd.is_alive:
        httpd.handle_request()
