const response_id = settings.response_id;

function updateBody({ body }) {
    if (body) {
        for (const {id, value} of body) {
            document.getElementById(id).outerHTML = value;
        }
    }
}

async function sendData(data) {
    let self = this;

    if (this.currentWaitingPromise) {
        await this.currentWaitingPromise;
    }

    this.currentWaitingPromise = fetch(document.location.origin, {
        method: "POST",
        body: JSON.stringify({
            ...data,
            response_id,
        })
    });

    this.currentWaitingPromise.then((response) => response.json())
    .then((response) => {
        updateBody(response)
    })
}

function doAction(action, ...args) {
    sendData({
        args,
        action,
    })
}

fetch(document.location.origin + "/updates?response_id=" + response_id)
.then(response => {
    const reader = response.body.getReader();

    const sync = async () => {
        const decoder = new TextDecoder("utf-8")
        while (true) {
            const { value, done } = await reader.read();

            if (done) {
                // If we are disconnected it's very possible
                // that the script was just ended
                // Restart the page to show the browsers default
                // no connection message
                location.reload();
                break;
            }

            try {
                const response = JSON.parse(decoder.decode(value));

                updateBody(response);
            } catch (error) {
                // Ignore errors, we could have just got responses
                // That pilled up a bit
            }
        }
    };

    sync();
});
