#!python3

import sys
import pathlib
sys.path.append((pathlib.Path(__file__).parent.parent / 'src').as_posix())

from live_gui import draw, state
from live_gui import (
    button,
    text,
    menu_bar,
)

def render_page_1():
    text("First page")

def render_page_2():
    text("Second page")

@state
class State:
    page = 1

@draw("Example app")
def myApp(state: State):
    with menu_bar():
        if button("First page"):
            state.page = 1
        if button("Second page"):
            state.page = 2

    if state.page == 1:
        render_page_1()
    elif state.page == 2:
        render_page_2()
