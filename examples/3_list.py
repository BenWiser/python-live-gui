#!python3

import sys
import pathlib
sys.path.append((pathlib.Path(__file__).parent.parent / 'src').as_posix())

from live_gui import state, draw
from live_gui import (
    a_list,
    button,
    column,
    list_item,
    row,
    text,
)

@state
class State:
    items = []

@draw("Example App")
def myApp(state: State):
    with column():
        text("This demo demonstrates how you can render a list")
        # This will add a new item to the list
        # updating the list will cause it to rerender
        if button("Add to list"):
            state.items.append("Item #%d" % len(state.items))

    with column():
        with a_list():
            for index, item in enumerate(state.items):
                with list_item():
                    with row():
                        text(item)
                        
                        # Updating a list item will only rerender this
                        # item
                        if button("Update list item name"):
                            state.items[index] += " and more"
